class Greeting:

    def __init__(self, name="Jeremy", greeting_message=" Hello!"):
        self.name = name
        self.greetingMessage = greeting_message

    def greet(self):
        return self.greetingMessage + self.name
