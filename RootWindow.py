from tkinter import *


class RootWindow(Frame):

    def __init__(self, master, greeter):
        """ Initialize frame"""
        super(RootWindow, self).__init__(master)
        self.grid()
        self.greeter = greeter
        self.create_widgets()


    def create_widgets(self):

        text = Text(self, width=100, height=20)
        text.insert('1.0', self.greeter.greet())
        text.grid()
